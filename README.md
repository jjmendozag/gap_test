Prueba Programada GAP - José Joaquin Mendoza Guevara
====================================================

Este proyecto esta programado utilizando Symfony Fullstack 2.3, fue
creado para cumplir los requerimientos de la prueba programada de GAP

IMPORTANTE: Para ejecutar el proyecto se nececsita al menos la version 5.3.9 de
PHP

1) Configuración Inicial
----------------------------------

Symfony utiliza composer como gestor de depencias, como buena practica no se 
agregan los vendors a los repositorios.

Debido a esto se debe correr el comando composer update en el folder raiz del 
proyecto (la misma donde se encuentra el archivo composer.json)

Por ejemplo en Mac OSX el comando se ejecuta de la siguiente manera:
    
    composer update

Esto instalara las depencias del proyecto incluyendo Symfony en el folder
de su proyecto en el subdirectorio "vendor".

### En caso de no tener composer instalado

Si no tiene Composer aun, decarguelo siguiendo las instrucciones de la pagina
http://getcomposer.org/ o solo corra el siguiente comando:

    curl -s http://getcomposer.org/installer | php


2) Revisar la configuración del Sistema
---------------------------------------

Ejecute  `check.php` script desde la linea de comandos:

    php app/check.php

Este comando devolvera una lista de todos los requerimientos necesarios para 
ejecutar la aplicacion
Si tiene algun error, arreglelos antes de continuar con la ejecucion.

3) Configuracion de Apache
--------------------------

Para poder ejecutar el proyecto a nivel de servidor apache es necesario crear un
Virual Host. Para dicho virtual host el web root debera de apuntar a la carpeta
/web del proyecto

4) Configuración de la Base de datos
------------------------------------

En la raiz del proyecto de encuentra el archivo:
    gap_test.sql.gz

Este archivo es un respaldo de la Base de Datos necesaria para ejecutar el 
proyecto, esta base de datos solo incluye las tablas y sus datos, por lo cual 
para hacer la migracion se debe de crear Schema de MySQL y una vez con el
schema creado para el proyecto seleccionado importar el archivo gap_test.sql.gz

En la carpeta raiz_del_proyecto/config se encuenta el archivo:
    
    parameters.yml

Este archivo incluye la configuracion de la conección a la base de datos,
a continuacion se pueden ver los parametros que deben de ser cambiados por los
de su conección:
    
    database_host: 127.0.0.1 #El host de la base de datos
    database_port: null      #El puerto, null si se utiliza el puerto por defecto
    database_name: gap_test  #El nombre del Schema de MySQL
    database_user: root      #El username
    database_password: root  #El password, null si no hay uno asignado

5) Ingreso al Backend
---------------------

Para ingresar al backend se utiliza la siguiente ruta:

    http://host_url/admin

Aqui sera necesario ingregar un username y password (Se hizo con seguridad 
porque el Sonata Bundle que fue el que se utilizo para crear el backend del 
proyecto tiene la seguridad integrada), los valores para ingresar son:

    Username = username
    Password = password

6) Llamada de los webservices
-----------------------------

Las rutas para utilizar los webservices son las siguientes:

    http://host_url/services/stores
    http://host_url/services/articles
    http://host_url/services/articles/stores/:id  #:id debe de ser cambiado por un id numerico

Todos los web services son seguros, la seguridad se implemento utilizando 
HTTP_BASIC_AUTHENTICATION, tal y como se solicita en el documento Services API.
El usuario utilizado es "my_user" y el password "my_password".
El formato del encabezado es el siguiente:

    Authorization: Basic bXlfdXNlcjpteV9wYXNzd29yZA==

En donde lo que sigue de basic es la hilera myuser:my_password encriptado en 
base 64.

Si el encabezado "Authorization" no es suministrado de la manera anteriormente 
indicada en los webservices, se obtendra un error 401.

7)Revisión del codigo
---------------------

Los controladores del Services API se encuentra en la carpeta:
    
    src/Base/AppBundle/Controller
    
Los archivos de las entidades que utiliza el ORM (Doctrine) se encuentran en la 
folder:

    src/Base/AppBundle/Entity

El header y el footer solicitados en los requerimientos se hicieron 
sobrecargando el layout basico de Sonata, los archivos modificados se pueden ver
en el folder:
    
    app/Resources/SonataAdminBundle/views
    
Los archivos relacionados  con las pruebas de PHP Unit se pueden ver en el 
siguiente folder:

   src/Base/AppBundle/Test/Controller

8) Ejecucion de las pruebas de PHP UNIT
---------------------------------------

Se programaron pruebas para el servicio http://host_url//services/stores
Para ejecutar estas pruebas se necesita tener instalado phpUnit, en caso de no 
tenerlo instalado se debe de instalar siguiendo las instrucciones de:

    https://phpunit.de/manual/current/en/installation.html

Una se tiene instalado PhpUnit, se de debe de  ejecutar en la raiz de proyecto:

    phpUnit -c app/

En caso de tener errores estos se deben a la version del php de sistema, 
ya que se necesita php 5.3.3,  es posible evadir estos errores si se corre el 
comando de la siguiente manera:

    php /usr/local/bin/phpunit -c app/

El comando anterior se encarga de ejecutar los Test de PhpUnit que se 
programaron para el servicio http://host_url/services/articles los test que 
ejecuta verifican que el servicio devuelva los valores correctos cuando el 
usuario envia los Authorization Headers correctos, y el segundo verifica que se 
obtenga el error custom 401 cuando los Authorization Headers estan incorrectos.

La respuesta esperada de no haber errores es:

    OK (2 tests, 4 assertions)

9) NOTAS
--------

Para la creacion del API REST se utilizo el "FOS Rest Bundle" de Symfony este 
tiene un pequeño bug que no permite deshabilitar el que el Bundle espere
 el formato de la respuesta del servicio, el servicio que realiza esto espera al
final de la URL un "." seguido de la extension. Para no tener que enviar siempre 
el formato de la respuesta se configuro para que cuando no lo reciba devuelva un
JSON.

Por ejemplo si para la llamada al api de /services/articles enviamos 
/services/articles.xml el bundle processa ".xml" y con esto sabe el formato 
esperado para la respuesta, y en lugar de devolver un JSON con la respuesta 
devuelve un XML. Ya que este funcionamiento no se puede deshabilitar, cuando 
se hace una llamada al servicio /services/articles/stores/:id pasando en lugar 
de id un valor con un punto como puede ser 1.5,  por el bug indicado 
anteriormente en lugar de obtener un error 400 de bad request como se indico 
en el documento "Services API" se obtiene una respuesta de HTML con el error 
404 de Symfony.
	