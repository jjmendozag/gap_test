<?php

namespace Base\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ArticleAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('id')
                ->add('name')
                ->add('description')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('id')
                ->add('name', null, array('editable' => true))
                ->add('store')
                ->add('description', null, array('editable' => true))
                ->add('price', null, array('editable' => true))
                ->add('totalInShelf', null, array('editable' => true))
                ->add('totalInVault', null, array('editable' => true))
                ->add('created_at')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name')
                ->add('description')
                ->add('price', 'number')
                ->add('totalInShelf')
                ->add('totalInVault')
                ->add('store', 'sonata_type_model', array(
                    'required' => true,
                    'expanded' => false,
                    'multiple' => false
                ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('description')
                ->add('price')
                ->add('totalInShelf')
                ->add('totalInVault')
                ->add('created_at')
                ->add('updated_at')
        ;
    }

}
