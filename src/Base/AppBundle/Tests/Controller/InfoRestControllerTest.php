<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Base\AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
/**
 * Description of InfoRestControllerTest
 *
 * @author Jose Mendoza
 */
class InfoRestControllerTest extends WebTestCase
{
    public function testSuccessGetArticles()
    {
        $client  = static::createClient();
        $headers = array('HTTP_Authorization' => 'Basic bXlfdXNlcjpteV9wYXNzd29yZA==');
        
        $client->request('GET', '/services/articles', array(), array(), $headers);
        
        $response = $client->getResponse();
        
        $this->assertTrue($response->getStatusCode() == 200);
        $this->assertContains('"success":true', $response->getContent());
    }
    
    public function testBadCredentialsGetArticles()
    {
        $client  = static::createClient();
        $headers = array('HTTP_Authorization' => 'Basic FClfdXNlcjpteV9wYXNzd29yYB==');
        
        $client->request('GET', '/services/articles', array(), array(), $headers);
        
        $response = $client->getResponse();
        
        $this->assertTrue($response->getStatusCode() == 200);
        $this->assertContains('"error_code":401', $response->getContent());
    }
}
