<?php

namespace Base\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use FOS\UserBundle\Model\UserInterface;
use Application\Sonata\UserBundle\Entity\User;

const USERNAME_AUTH = 'my_user';
const PASSWORD_AUTH = 'my_password';

class InfoRestController extends Controller {

    /**
     * @Rest\Get("/stores")
     * @Rest\View()
     */
    public function getStoresAction() {

        if (!$this->validateUser($this->getRequest()->headers->get('authorization'))) {
            return $this->getErrorByCode(401);
        }

        $repo = $this->getDoctrine()->getRepository("BaseAppBundle:Store");
        $stores = $repo->findAll();
        return array('success' => true, 'stores' => $stores, 'total_elements' => count($stores));
    }

    /**
     * @Rest\Get("/articles/stores/{id}")
     * @Rest\View()
     */
    public function getArticlesByStoreAction($id) {

        if (!$this->validateUser($this->getRequest()->headers->get('authorization'))) {
            return $this->getErrorByCode(401);
        }

        if (!filter_var($id, FILTER_VALIDATE_INT)) {
            return $this->getErrorByCode(400);
        }

        $repo = $this->getDoctrine()->getRepository("BaseAppBundle:Store");
        $store = $repo->findOneById($id);

        if (!$store) {
            return $this->getErrorByCode(404);
        }

        $articles = $store->getArticles();
        return array('success' => true, 'articles' => $articles, 'total_elements' => count($articles));
    }

    /**
     * @Rest\Get("/articles")
     * @Rest\View()
     */
    public function getArticlesAction() {

        if (!$this->validateUser($this->getRequest()->headers->get('authorization'))) {
            return $this->getErrorByCode(401);
        }

        $repo = $this->getDoctrine()->getRepository("BaseAppBundle:Article");
        $articles = $repo->findAll();
        return array('success' => true, 'articles' => $articles, 'total_elements' => count($articles));
    }

    public function validateUser($authorization) {
        if ($authorization) {
            try {
            if (strpos(strtolower($authorization), 'basic') === 0) {
                list($username, $password) = explode(':', base64_decode(substr($authorization, 6)));
                if ((USERNAME_AUTH == $username) && (PASSWORD_AUTH == $password)) {
                    return true;
                }
            }
            } catch (Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function getErrorByCode($code = 401) {
        switch ($code) {
            case 401:
                return array("error_msg" => "Not authorized", "error_code" => 401, "success" => false);

            case 404:
                return array("error_msg" => "Record not Found", "error_code" => 404, "success" => false);

            default:
                return array("error_msg" => "Bad Request", "error_code" => 400, "success" => false);
        }
    }

}
