<?php

namespace Base\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="Base\AppBundle\Entity\ArticleRepository")
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("all") 
 */
class Article {

   public function __toString() {
       return $this->getName();
   }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\SerializedName("id")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\SerializedName("name")
     * @Serializer\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Serializer\SerializedName("description")
     * @Serializer\Expose
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=16, scale=2)
     * @Serializer\SerializedName("price")
     * @Serializer\Expose
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_in_shelf", type="integer")
     * @Serializer\SerializedName("total_in_shelf")
     * @Serializer\Expose
     */
    private $totalInShelf;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_in_vault", type="integer")
     * @Serializer\SerializedName("total_in_vault")
     * @Serializer\Expose
     */
    private $totalInVault;

    /**
     * @ORM\ManyToOne(targetEntity="\Base\AppBundle\Entity\Store", inversedBy="articles")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     */
    private $store;
    
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;


    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("store_name")
     * @return array
     */
    public function getStoreName(){
        return $this->getStore()->getName();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Article
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set totalInShelf
     *
     * @param integer $totalInShelf
     * @return Article
     */
    public function setTotalInShelf($totalInShelf)
    {
        $this->totalInShelf = $totalInShelf;
    
        return $this;
    }

    /**
     * Get totalInShelf
     *
     * @return integer 
     */
    public function getTotalInShelf()
    {
        return $this->totalInShelf;
    }

    /**
     * Set totalInVault
     *
     * @param integer $totalInVault
     * @return Article
     */
    public function setTotalInVault($totalInVault)
    {
        $this->totalInVault = $totalInVault;
    
        return $this;
    }

    /**
     * Get totalInVault
     *
     * @return integer 
     */
    public function getTotalInVault()
    {
        return $this->totalInVault;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Article
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set store
     *
     * @param \Base\AppBundle\Entity\Store $store
     * @return Article
     */
    public function setStore(\Base\AppBundle\Entity\Store $store = null)
    {
        $this->store = $store;
    
        return $this;
    }

    /**
     * Get store
     *
     * @return \Base\AppBundle\Entity\Store 
     */
    public function getStore()
    {
        return $this->store;
    }
}